import { action, makeObservable, observable } from 'mobx'
import { Observer } from 'mobx-react'
import parseHTML from 'html-react-parser'
import React from 'react'

const LOCALES_DIR = `./locales`

export const getLanguageLabel = (language: string): string => {
    let label: string
    try {
        label = `${new Intl.DisplayNames([language], { type: 'language' }).of(language)}`
    } catch (e) {
        label = `${new Intl.DisplayNames(['fr'], { type: 'language' }).of('fr')}`
    }
    return label.charAt(0).toUpperCase() + label.slice(1)
}

export interface Language {
    label?: string
    flag?: any
    translations: { [value: string]: string }
}

export interface Languages {
    [language: string]: Language
}

interface GettextOptions {
    html?: boolean
}

interface GettextOptions {
    html?: boolean
}

export const _ = (value: string, options: GettextOptions = {}): React.ReactNode => {
    return typeof window === 'undefined' ? (
        parseHTML(i18n.getTranslation(value))
    ) : (
        <Observer>{(): any => parseHTML(i18n.getTranslation(value))}</Observer>
    )
}

class I18n {
    //
    language: string = 'en'
    languages: Languages = {}
    availableLanguages: string[] = (process.env.NEXT_PUBLIC_LANGUAGES ?? '').split(',')

    constructor() {
        Object.entries(this.languages).map(([language, data]) => {
            data.label = getLanguageLabel(language)
        })
        makeObservable(this, {
            language: observable,
            languages: observable,
            setLanguage: action,
            loadLanguage: action,
        })

        this.loadLanguages()
    }

    setLanguage(language: string): void {
        this.language = language
    }

    getTranslation(value: string): string {
        let trans = this.languages[this.language]?.translations?.[value] ?? ''
        return trans != '' ? trans : value
    }

    mapLanguages(iteratee: (data: Language & { key: string; selected: boolean }) => React.ReactNode): React.ReactNode {
        const iterateeProxy = () =>
            Object.entries(this.languages).map(([key, data]) => iteratee({ ...data, key, selected: key === this.language }))
        return typeof window === 'undefined' ? iterateeProxy() : <Observer>{(): any => iterateeProxy()}</Observer>
    }

    loadLanguage(key: string, language: Language): void {
        this.languages = { ...this.languages, [key]: language }
    }
    loadLanguages(): void {
        this.availableLanguages.map(async (language) => {
            const translations = (await import(`${LOCALES_DIR}/${language}.json`)).default
            this.loadLanguage(language, {
                translations,
                label: getLanguageLabel(language),
            })
        })
    }
}

export const i18n = new I18n()
