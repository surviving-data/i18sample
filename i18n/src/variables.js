const localesPath = '/locales'
const sourcesPath = '/sources'
const waitThresholdDefault = 5000
const waitThresholdAfterBannishment = 10000

module.exports = {
    localesPath,
    sourcesPath,
    waitThresholdDefault,
    waitThresholdAfterBannishment,
}
