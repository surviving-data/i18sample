const util = require('util')
const glob = util.promisify(require('glob'))
const execSync = require('child_process').execSync
const fs = require('fs')

// Relatives
const { localesPath, sourcesPath } = require('./variables')
const { getLanguages, getFormat } = require('./utils')
const potFile = `${localesPath}/_.pot`

const mergePot = async () => {
    //
    // get all files contains __('xxxxxx') to translate
    const anySources = await glob(`${sourcesPath}/**/*.@(ts|tsx|js|py)`, {})
    const sources = [...anySources]

    // Generate POT (empty msgstr, just for created and deleted translations merging) with xgettext
    console.log(`Generating POT file: ${potFile} from ${sources.length} files`)
    !fs.existsSync(potFile) && fs.createWriteStream(potFile)
    if (sources.length > 0) {
        execSync(`xgettext --language=Python --from-code=utf-8 --keyword=__ -c -o ${potFile} ${sources.join(' ')}`)
    } else {
        console.error('>>>> No sources <<<<')
    }
}

// Create / Update all the PO files hightly sync
const mergePo = async (language) => {
    //
    // Get the related path to the po file
    const poFile = `${localesPath}/${language}.po`

    // Define locale in gettext format
    let locale = language.replace('-', '_')

    // Create the PO file if doesn't exist and froce UTF-8 Charset
    // console.log(`Generating PO ${poFile} for ${locale}`)
    !fs.existsSync(poFile) && execSync(`msginit --no-translator -i ${potFile} -l ${locale} -o ${poFile}`)
    fs.writeFileSync(poFile, fs.readFileSync(poFile).toString().replace('charset=ASCII', 'charset=UTF-8'))

    // Merge duplicates msgid
    execSync(`msguniq ${poFile} --unique  -o ${poFile}`)

    // Merge the POT contains created and deleted translations to the PO
    execSync(`msgmerge ${poFile} ${potFile} --no-fuzzy-matching -o ${poFile}`)

    // Empty fuzzy legacy
    execSync(`msgattrib ${poFile} --clear-fuzzy --empty -o ${poFile}`)

    console.log(`Generating JSON ${poFile} for ${locale}`)

    const format = getFormat()

    if (format == 'po') {
    } else if (format == 'mo') {
        // TODO: Compile
    } else {
        // TODO: in  mf, raw, jed, jed1.x
        try {
            const data = require('po2json').parseFileSync(poFile, {
                format,
            })
            fs.writeFileSync(`${localesPath}/${language}.json`, JSON.stringify(data))
        } catch (e) {
            console.error(e)
        }
    }
}
// Create / Update all the PO files hightly sync
const mergeNewMessages = async () => {
    //
    // Merge references PO File
    await mergePot()

    // For every languages
    const languages = await getLanguages()
    languages.map(mergePo)
}

module.exports = {
    mergeNewMessages,
}
