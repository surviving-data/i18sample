function sample(choices) {
    var index = Math.floor(Math.random() * choices.length)
    return choices[index]
}

function randFloat(min, max) {
    return Math.random() * (max - min) + min
}

const getFormat = () => `${process.env.FORMAT || 'mf'}`
const getLanguages = () => `${process.env.LANGUAGES || 'en,fr'}`.split(',')
const getDefaultLanguage = () => `${process.env.DEFAULT_LANGUAGE || 'en'}`

const getLocale = (language) => {
    let locale = `${language}`.toLowerCase()
    // Exception for chinese in simplfied mode
    if (['zh_CN', 'cn', 'zh'].includes(language)) {
        locale = 'zh-cn'
    } else {
        locale = language.split('_')[0]
    }
    return locale
}
module.exports = { sample, randFloat, getFormat, getLanguages, getDefaultLanguage, getLocale }
