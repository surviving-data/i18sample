
# ██████╗░███████╗██████╗░░██████╗
# ██╔══██╗██╔════╝██╔══██╗██╔════╝
# ██║░░██║█████╗░░██████╔╝╚█████╗░
# ██║░░██║██╔══╝░░██╔═══╝░░╚═══██╗
# ██████╔╝███████╗██║░░░░░██████╔╝
# ╚═════╝░╚══════╝╚═╝░░░░░╚═════╝░
FROM node:19 AS deps
ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV:-"production"}
ENV NEXT_TELEMETRY_DISABLED=1
#ENV NPM_URL="gitlab.com/api/v4/projects/${PROJECT_ID}/packages/npm/"
#ARG GITLAB_TOKEN
#ENV NPM_TOKEN=${GITLAB_TOKEN}
#RUN echo "@lp:registry=https://${NPM_URL}" > ~/.npmrc
#RUN echo "//${NPM_URL}:_authToken=$NPM_TOKEN" >> ~/.npmrc
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn set version latest
RUN mkdir -p ./node_modules
RUN if [ "$NODE_ENV" = "production" ]; then yarn install --frozen-lockfile; fi


# ░██████╗████████╗░█████╗░██████╗░████████╗
# ██╔════╝╚══██╔══╝██╔══██╗██╔══██╗╚══██╔══╝
# ╚█████╗░░░░██║░░░███████║██████╔╝░░░██║░░░
# ░╚═══██╗░░░██║░░░██╔══██║██╔══██╗░░░██║░░░
# ██████╔╝░░░██║░░░██║░░██║██║░░██║░░░██║░░░
# ╚═════╝░░░░╚═╝░░░╚═╝░░╚═╝╚═╝░░╚═╝░░░╚═╝░░░
FROM node:19 AS builder
ENV TSC_COMPILE_ON_ERROR=true
ARG NODE_ENV
ENV NODE_ENV=${NODE_ENV:-"production"}
ENV REACT_EDITOR=atom
ENV NEXT_TELEMETRY_DISABLED=1
WORKDIR /app
COPY --from=deps /app/node_modules ./node_modules
COPY . .
RUN if [ "$NODE_ENV" = "production" ]; then yarn run build; fi
EXPOSE 8080
CMD ["next", "start", "-p", "8080" ,"-H", "0.0.0.0"]



