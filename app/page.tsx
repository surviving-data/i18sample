"use client";

import Image from "next/image";
import styles from "./page.module.css";
import { _, i18n } from "@/i18n";
import ClientSideComponent from "./ClientSideComponent";

i18n.setLanguage("en");

export default function Home() {
  return (
    <main className={styles.main}>
      <div className={styles.description}>
        <p>
          {_("Get started by editing")}&nbsp;
          <code className={styles.code}>app/page.tsx</code>
        </p>
        <div>
          <a
            href="https://vercel.com?utm_source=create-next-app&utm_medium=appdir-template&utm_campaign=create-next-app"
            target="_blank"
            rel="noopener noreferrer"
          >
            {_("By")}{" "}
            <Image
              src="/vercel.svg"
              alt="Vercel Logo"
              className={styles.vercelLogo}
              width={100}
              height={24}
              priority
            />
          </a>
        </div>
      </div>
      <div style={{ display: "grid", zIndex: 1000, position: "relative" }}>
        Languages select:
        {i18n.mapLanguages(({ key, label, selected }) => (
          <button
            key={key}
            onClick={() => i18n.setLanguage(key)}
            style={{ border: selected ? "3px solid red" : "" }}
          >
            {label} <img src={`/flags/${key}.png`} style={{ height: "1rem" }} />
          </button>
        ))}
      </div>
      <ClientSideComponent />
    </main>
  );
}
