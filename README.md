# Start the demo

```bash
docker compose build && docker compose run --rm front yarn install && PORT=8003 docker compose up
```

then just open http://localhost:8003 (or the port you set bellow, PORT=XXXX)

# Implement into your project

All the logics are in ./i18n/index.tsx ~90 lines of code

Define environment variables

```bash
NEXT_PUBLIC_LANGUAGES=fr,en,es,ro,zh,ko,sv,nl,it,ca,bn,...the languages you want....
```

Codes are ISO 639-2 => https://en.wikipedia.org/wiki/List_of_ISO_639-2_codes

Add mobx mobx-react html-react-parser

```bash
yarn add mobx mobx-react html-react-parser
```

Copy the i18n folder from this repo into your project, then anywhere you want, import the "\_" gettext styled function

```ts
import { _ } from "./i18n";
```

At this point you already have a magic mobxed i18n system, just use some `_('blabla')` and it will work

# Launch the auto translator service

Configure a `docker-compose.yml` with the following content:

```yaml
version: "3.9"
services:
  i18n:
    build: ./i18n # The folder where i18n is
    environment:
      DEFAULT_LANGUAGE: en # Define the default reference language
      LANGUAGES: fr,en,es,ro,zh,ko,sv,nl,it,ca,bn # Define the languages to generate
      FORMAT: mf # po, mo, raw, jed, jed1.x # Define the format of the locales files
    volumes:
      - ./i18n/locales:/locales # Define where stored the locales files
      - ./app/:/sources/front/ # Define where stored the sources files (watched)
    restart: unless-stopped
```

then

```bash
docker compose run --rm i18n
```
